﻿using System;

namespace Solution1
{
    public class Node
    {
        public Node LeftChild { get; private set; }
        public Node RightChild { get; private set; }

        public Node(Node leftChild, Node rightChild)
        {
            this.LeftChild = leftChild;
            this.RightChild = rightChild;
        }

        private int FindHeight(Node root)
        {
            if (root.LeftChild == null && root.RightChild == null)
            {
                return 0; // A root with no children has a height of 0
            }

            else if (root.LeftChild == null)
            {
                return 1 + FindHeight(root.RightChild);
            }

            else if (root.RightChild == null)
            {
                return 1 + FindHeight(root.LeftChild);
            }

            else
            {
                return 1 + Math.Max(FindHeight(root.LeftChild), FindHeight(root.RightChild));
            }

        }

        public int Height()
        {
            // throw new NotImplementedException("Waiting to be implemented.");
            return FindHeight(this);
        }

        static void Main(string[] args)
        {
            Node leaf1 = new Node(null, null);
            Node leaf2 = new Node(null, null);
            Node node = new Node(leaf1, null);
            Node root = new Node(node, leaf2);

            Console.WriteLine(root.Height());
        }
    }
}
