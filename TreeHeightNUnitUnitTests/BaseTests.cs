﻿using NUnit.Framework;
using System;

namespace TreeHeightNUnitUnitTests
{
    internal abstract class BaseTests
    {
        #region Activator Support
        public string FullyQualifiedClassName { get; set; }

        private object NewClassInstance(string fullyQualifiedName, dynamic leaf1, dynamic leaf2)
        {

            Type type = Type.GetType(fullyQualifiedName);

            if (type != null)
                return Activator.CreateInstance(type, args: new object[] { leaf1, leaf2 });

            return null;
        }
        #endregion

        [Test]
        public void ExampleCase()
        {
            dynamic leaf1 = NewClassInstance(FullyQualifiedClassName, null, null);
            dynamic leaf2 = NewClassInstance(FullyQualifiedClassName, null, null);
            dynamic node = NewClassInstance(FullyQualifiedClassName, leaf1, null);
            dynamic root = NewClassInstance(FullyQualifiedClassName, node, leaf2);

            Assert.AreEqual(2, root.Height());
        }


        [Test]
        public void SmallTree()
        {
            dynamic leaf1 = NewClassInstance(FullyQualifiedClassName, null, null);
            dynamic leaf2 = NewClassInstance(FullyQualifiedClassName, null, null);
            
            dynamic branch1 = NewClassInstance(FullyQualifiedClassName, leaf1, null);
            dynamic branch2 = NewClassInstance(FullyQualifiedClassName, leaf1, leaf2);
            dynamic arm1 = NewClassInstance(FullyQualifiedClassName, branch1, branch2);

            dynamic branch3 = NewClassInstance(FullyQualifiedClassName, null, null);
            dynamic branch4 = NewClassInstance(FullyQualifiedClassName, null, leaf2);
            dynamic arm2 = NewClassInstance(FullyQualifiedClassName, branch3, branch4);

            dynamic level1 = NewClassInstance(FullyQualifiedClassName, arm1, arm2);
            dynamic level2 = NewClassInstance(FullyQualifiedClassName, arm2, arm2);

            dynamic sublevel1 = NewClassInstance(FullyQualifiedClassName, level1, level1);
            dynamic sublevel2 = NewClassInstance(FullyQualifiedClassName, level2, level2);

            dynamic root = NewClassInstance(FullyQualifiedClassName, sublevel1, sublevel2);

            Assert.AreEqual(5, root.Height());
        }

        [Test]
        public void Leaf()
        {
            dynamic leaf1 = NewClassInstance(FullyQualifiedClassName, null, null);
            
            dynamic root = NewClassInstance(FullyQualifiedClassName, leaf1, leaf1);

            Assert.AreEqual(1, root.Height());
        }

    }
}
