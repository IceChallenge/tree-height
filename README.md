Visual Studio 2019 .NET Core 3.1 Console Application in C#

# Projects

    TreeHeightCodeChallenge - Code Challenge for Developer

    TreeHeightNUnitUnitTests - Test cases you can run against the Code Challenge
                              - Test cases you can run against Solution1 to see how each solution works

    Solution1 - One solution that solves each Test Case

# Instructions

    Open the "TreeHeightCodeChallenge" Project    
    Set your timer at 60:00 minutes and then start this exercise with the details below
    Run Unit Tests for "TestCasesForChallenge" to test your results

# Code Challenge

    A tree is an abstract data structure consisting of nodes.  Each node has only one parent node and zero or more child nodes.  Each tree has one special node, called a root, which has no parent node.  A node is called a leaf if it has no child nodes.  The height of a tree is the largest distance (number of parent-child connections) from the root to a leaf.

    Write a function that calculates the height of a given tree.  For example, the tree below has a height of 2 since the longest distance (number of parent-child connections) from the root to a leaf is 2 (the connections are the root to a node and then the node to leaf 1):

        Node leaf1 = new Node(null, null);
        Node leaf2 = new Node(null, null);
        Node node = new Node(leaf1, null);
        Node root = new Node(node, leaf2);